/**
 * This package is used in addition to Spring MVC. It is replacing default Spring MVC HTTP 400 Bad request errors with
 * custom format.
 */
package BANK.web.restapi.commonvalidation;
