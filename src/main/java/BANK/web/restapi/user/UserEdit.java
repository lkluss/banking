/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BANK.web.restapi.user;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;



/**
 *
 * @author Mateusz
 */
public class UserEdit {
   
   @NotNull
   private Long userId;
   
   @NotEmpty
   private String firstname;
   
   @NotEmpty
   private String lastname;

   public Long getUserId() {
      return userId;
   }

   public void setUserId(Long userId) {
      this.userId = userId;
   }

   public String getFirstname() {
      return firstname;
   }

   public void setFirstname(String firstname) {
      this.firstname = firstname;
   }

   public String getLastname() {
      return lastname;
   }

   public void setLastname(String lastname) {
      this.lastname = lastname;
   }
      
   
}
