/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BANK.web.restapi.user;

import org.hibernate.validator.constraints.NotEmpty;


/**
 *
 * @author Mateusz
 */

public class UserNew {
   
   @NotEmpty
   private String firstname;
   
   @NotEmpty
   private String lastname;

   public String getFirstname() {
      return firstname;
   }

   public void setFirstname(String firstname) {
      this.firstname = firstname;
   }

   public String getLastname() {
      return lastname;
   }

   public void setLastname(String lastname) {
      this.lastname = lastname;
   }

   
}
