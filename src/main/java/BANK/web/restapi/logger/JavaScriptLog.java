package BANK.web.restapi.logger;



public class JavaScriptLog {

   private String url;

   private String message;

   private String cause;

   private String stackTrace;


   public String getUrl() {
      return url;
   }

   public void setUrl(String url) {
      this.url = url;
   }


   public String getMessage() {
      return message;
   }

   public void setMessage(String message) {
      this.message = message;
   }


   public String getCause() {
      return cause;
   }

   public void setCause(String cause) {
      this.cause = cause;
   }

   public String getStackTrace() {
      return stackTrace;
   }

   public void setStackTrace(String stackTrace) {
      this.stackTrace = stackTrace;
   }

}
