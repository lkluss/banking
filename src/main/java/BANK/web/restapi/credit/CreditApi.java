package BANK.web.restapi.credit;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import BANK.domain.credit.dto.CreditSnapshot;
import BANK.domain.credit.finder.ICreditSnapshotFinder;

import com.fasterxml.jackson.annotation.JsonView;

import BANK.domain.credit.bo.ICreditBO;

@RestController
@RequestMapping("/api/credit")
public class CreditApi {

   
   private final ICreditSnapshotFinder creditSnapshotFinder;

   private final ICreditBO creditBO;

   @Autowired
   public CreditApi(ICreditSnapshotFinder creditSnapshotFinder, ICreditBO creditBO) {
      this.creditSnapshotFinder = creditSnapshotFinder;
      this.creditBO = creditBO;
   }


   @RequestMapping(method = RequestMethod.GET)
   public List<Credit> list() {
      List<CreditSnapshot> creditSnapshots = creditSnapshotFinder.findAll();

      return creditSnapshots.stream()
         .map(Credit::new)
         .collect(Collectors.toList());
   }


   @RequestMapping(value = "/active",
      method = RequestMethod.GET)
   public List<Credit> active() {
      List<CreditSnapshot> creditSnapshots = creditSnapshotFinder.findActive();

      return creditSnapshots.stream()
         .map(Credit::new)
         .collect(Collectors.toList());
   }


   public HttpEntity<Credit> get(@PathVariable("id") long id) {
      CreditSnapshot creditSnapshot = creditSnapshotFinder.findById(id);

      if (creditSnapshot == null) {
         return new ResponseEntity<>(HttpStatus.NOT_FOUND);
      } else {
         return new ResponseEntity<>(new Credit(creditSnapshot), HttpStatus.OK);
      }
   }


   @RequestMapping(method = RequestMethod.POST,
      consumes = MediaType.APPLICATION_JSON_VALUE)
   public HttpEntity<Credit> add(@RequestBody CreditNew creditNew) {
         
      CreditSnapshot creditSnapshot = creditBO.add(creditNew.getStartDate(),
         creditNew.getFinishDate(),creditNew.getProvision(), creditNew.getInstallment(), creditNew.getRateOfInterest(),
         creditNew.getInterest(), creditNew.isInsuranace());

      return new ResponseEntity<>(new Credit(creditSnapshot), HttpStatus.OK);
   }


   @RequestMapping(method = RequestMethod.PUT,
      consumes = MediaType.APPLICATION_JSON_VALUE)
   public HttpEntity<Credit> update(
      @RequestBody CreditEdit creditEdit) {

      CreditSnapshot foundedCredit = creditSnapshotFinder.findById(creditEdit.getCreditId());

      CreditSnapshot creditSnapshot = creditBO.edit(creditEdit.getCreditId(),creditEdit.getStartDate(),
         creditEdit.getFinishDate(),creditEdit.getProvison(), creditEdit.getInstallment(), creditEdit.getRateOfInterest(),
         creditEdit.getInterest(), creditEdit.isInsuranace());

      return new ResponseEntity<>(new Credit(creditSnapshot), HttpStatus.OK);
   }

   @RequestMapping(value = "finish/{id}",
      method = RequestMethod.PUT)
   public HttpEntity<Credit> finish(@PathVariable("id") Long creditId) {
      CreditSnapshot creditSnapshot = creditSnapshotFinder.findById(creditId);
      
      if (creditId != null ) {
         creditBO.finish(creditId);
         return new ResponseEntity<>(new Credit(creditSnapshot), HttpStatus.OK);
      }

      return new ResponseEntity<>(new Credit(creditSnapshot), HttpStatus.NOT_FOUND);
   }

   @RequestMapping(value = "delete/{id}",
      method = RequestMethod.DELETE)
   public HttpEntity delete(@PathVariable("id") Long creditId) {
      CreditSnapshot creditSnapshot = creditSnapshotFinder.findById(
         creditId);
      
      if (creditId != null) {
         creditBO.delete(creditId);
      }

      return new ResponseEntity<>(new Credit(creditSnapshot), HttpStatus.OK);
   }
}
