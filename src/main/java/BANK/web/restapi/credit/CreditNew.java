/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BANK.web.restapi.credit;

import java.time.LocalDateTime;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import BANK.web.converter.LocalDateTimeDeserializer;
import BANK.web.restapi.annotations.RangeOfTimestamp;


/**
 *
 * @author Mateusz
 */

public class CreditNew {
   
   
   @RangeOfTimestamp
   private LocalDateTime startDate;
   
   @RangeOfTimestamp
   private LocalDateTime finishDate;
   
   @NotNull
   private Long provision;
   
   @NotNull
   private Long installment;
   
   @NotNull   
   private Double rateOfInterest;
   
   @NotNull   
   private Double interest;
   
   private boolean insurance;

   public LocalDateTime getStartDate() {
      return startDate;
   }

   @JsonDeserialize(using = LocalDateTimeDeserializer.class)
   public void setStartDate(LocalDateTime startDate) {
      this.startDate = startDate;
   }

   public LocalDateTime getFinishDate() {
      return finishDate;
   }

   @JsonDeserialize(using = LocalDateTimeDeserializer.class)
   public void setFinishDate(LocalDateTime finishDate) {
      this.finishDate = finishDate;
   }

   public Long getProvision() {
      return provision;
   }

   public void setProvision(Long provision) {
      this.provision = provision;
   }

   public Long getInstallment() {
      return installment;
   }

   public void setInstallment(Long installment) {
      this.installment = installment;
   }

   public Double getRateOfInterest() {
      return rateOfInterest;
   }

   public void setRateOfInterest(Double rateOfInterest) {
      this.rateOfInterest = rateOfInterest;
   }

   public Double getInterest() {
      return interest;
   }

   public void setInterest(Double interest) {
      this.interest = interest;
   }

   public boolean isInsuranace() {
      return insurance;
   }

   public void setInsuranace(boolean insurance) {
      this.insurance = insurance;
   }
   
}
