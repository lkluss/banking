/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BANK.web.restapi.credit;

import java.time.LocalDateTime;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;



/**
 *
 * @author Mateusz
 */
public class CreditEdit {
   
   @NotNull
   private Long creditId;
   
   //brakuje convertera
   private LocalDateTime startDate;
   
   private LocalDateTime finishDate;
   
   @NotNull
   private Long provision;
   
   @NotNull
   private Long installment;
   
   @NotNull   
   private Double rateOfInterest;
   
   @NotNull   
   private Double interest;
   
   private boolean insurance;

   public Long getCreditId() {
      return creditId;
   }

   public void setCreditId(Long creditId) {
      this.creditId = creditId;
   }

   public LocalDateTime getStartDate() {
      return startDate;
   }

   public void setStartDate(LocalDateTime startDate) {
      this.startDate = startDate;
   }

   public LocalDateTime getFinishDate() {
      return finishDate;
   }

   public void setFinishDate(LocalDateTime finishDate) {
      this.finishDate = finishDate;
   }

   public Long getProvison() {
      return provision;
   }

   public void setProvison(Long provision) {
      this.provision = provision;
   }

   public Long getInstallment() {
      return installment;
   }

   public void setInstallment(Long installment) {
      this.installment = installment;
   }

   public Double getRateOfInterest() {
      return rateOfInterest;
   }

   public void setRateOfInterest(Double rateOfInterest) {
      this.rateOfInterest = rateOfInterest;
   }

   public Double getInterest() {
      return interest;
   }

   public void setInterest(Double interest) {
      this.interest = interest;
   }

   public boolean isInsuranace() {
      return insurance;
   }

   public void setInsuranace(boolean insurance) {
      this.insurance = insurance;
   }   
   
}
