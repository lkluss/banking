package BANK.service.user;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import BANK.domain.user.dto.UserSnapshot;
import BANK.domain.user.finder.IUserSnapshotFinder;

@Component
public class LoggedUserService
   implements ApplicationContextAware {

   private static IUserSnapshotFinder userSnapshotFinder;

   @Override
   public void setApplicationContext(ApplicationContext applicationContext)
      throws BeansException {
      userSnapshotFinder = (IUserSnapshotFinder) applicationContext.getBean("userSnapshotFinder");
   }

   public static UserSnapshot getSnapshot() {
      if (userSnapshotFinder == null) {
         throw new IllegalStateException();
      }
      String username = SecurityContextHolder.getContext().getAuthentication().getName();
      return userSnapshotFinder.findByUsername(username);
   }

   public static List<String> getRoles() {
      return findUserRoles();
   }

   public static boolean hasAnyRole(String... roles) {
      List<String> hasRoles = findUserRoles();
      boolean result = false;
      for (String role : roles) {
         if (hasRoles.contains(role)) {
            result = true;
            break;
         }
      }
      return result;
   }

   private static List<String> findUserRoles() {
      return SecurityContextHolder.getContext()
         .getAuthentication()
         .getAuthorities()
         .stream()
         .map(Object::toString)
         .collect(Collectors.toList());
   }
   
}
