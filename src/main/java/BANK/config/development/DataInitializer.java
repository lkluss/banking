package BANK.config.development;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import BANK.domain.user.finder.IUserSnapshotFinder;
import BANK.sharedkernel.constant.Profiles;

@Component
@Profile(Profiles.DEVELOPMENT)
public class DataInitializer {

   @Autowired
   private IUserInitializer userInitializer;

   @Autowired
   private IUserSnapshotFinder userSnapshotFinder;

   @Transactional
   @PostConstruct
   public void init() {
      if (userSnapshotFinder.findAll().isEmpty()) {
         userInitializer.initalizer();
      }
   }

}
