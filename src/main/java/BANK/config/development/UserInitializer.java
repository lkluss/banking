package BANK.config.development;

import java.time.LocalDateTime;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import BANK.domain.user.bo.IUserBO;
import BANK.sharedkernel.constant.Profiles;

@Component
@Profile(Profiles.DEVELOPMENT)
public class UserInitializer
   implements IUserInitializer {

  @Autowired
   private IUserBO userBO;

   @Transactional
   @Override
   public void initalizer() {

      userBO.add(UUID.randomUUID(), "Mateusz", "Głąbicki",
         "Admin", "mateusz.glabicki",
         LocalDateTime.now(), "mateusz.glabicki@nsa.pl","ROLE_ADMIN","password");
      
      userBO.add(UUID.randomUUID(), "Mateusz", "Łędzewicz",
         "User", "mateusz.ledzewicz",
         LocalDateTime.now(), "mateusz.łędzewicz@nsa.pl","ROLE_USER","password");
      
      userBO.add(UUID.randomUUID(), "Mariusz", "Kłysiński",
         "User", "mariusz.klysinski",
         LocalDateTime.now(), "mariusz.klysinski@nsa.pl","ROLE_USER","password");
      
   }

}
