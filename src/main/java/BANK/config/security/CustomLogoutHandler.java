/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BANK.config.security;

import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.stereotype.Component;

import BANK.domain.user.bo.IUserBO;
import BANK.domain.user.dto.UserSnapshot;
import BANK.domain.user.finder.IUserSnapshotFinder;

/**
 *
 * @author Mateusz
 */
@Component
public class CustomLogoutHandler implements LogoutHandler {
   
    @Autowired
    private IUserSnapshotFinder userSnapshotFinder;
    @Autowired
    private IUserBO userBO;
      
    @Override
    public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
//      UserSnapshot emp = userSnapshotFinder.findOneByUsernameAndPassword(authentication.getName(),authentication.getCredentials().toString());
//      userBO.block(emp.getId());
    }
}
