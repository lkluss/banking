package BANK.domain.user.finder;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import BANK.domain.user.dto.UserSnapshot;

/**
 *
 * @author Mateusz.Glabicki
 */
public interface IUserSnapshotFinder {

   UserSnapshot findById(Long id);

   UserSnapshot findByUuid(UUID guid);

   List<UserSnapshot> findAll();

   UserSnapshot findByUsername(String username);

   List<UserSnapshot> findAll(Set<Long> ids);

   List<UserSnapshot> findActive();
   
   Map<Long, UserSnapshot> findAllAsMap(Set<Long> ids);
      
   UserSnapshot findOneByUsernameAndPassword(String name,String password);
}
