package BANK.domain.credit.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import BANK.config.persistance.converter.LocalDateTimePersistenceConverter;
import BANK.domain.credit.dto.CreditSnapshot;
import BANK.sharedkernel.exception.EntityInStateNewException;

/**
 *
 * @author Mateusz.Glabicki
 */
@Entity
public class Credit
   implements Serializable {

   private static final long serialVersionUID = 7972265007575707207L;

   @Id
   @GeneratedValue(strategy = GenerationType.AUTO)
   private Long id;

   @NotNull
   @Convert(converter = LocalDateTimePersistenceConverter.class)
   private LocalDateTime createdAt;
   
   @NotNull
   @Convert(converter = LocalDateTimePersistenceConverter.class)
   private LocalDateTime startDate;
   
   @NotNull
   @Convert(converter = LocalDateTimePersistenceConverter.class)
   private LocalDateTime finishDate;

   @Version
   private long version;
   
   @NotNull
   private Long provision;

   private Long installment;
   
   @NotNull
   private Double rateOfInterest;
   
   @NotNull
   private Double interest;
   
   private boolean active;
   
   private boolean insurance;


   protected Credit() {
   }

   public Credit( LocalDateTime startDate, LocalDateTime finishDate,
      Long provision, Long installment, Double rateOfInterest, Double interest, boolean insurance) {
      this.createdAt = LocalDateTime.now();
      // do zmiany 
      this.startDate = LocalDateTime.now();
      this.finishDate = LocalDateTime.now();
      this.version = 1;
      this.provision = provision;
      this.installment = installment;
      this.rateOfInterest = rateOfInterest;
      this.interest = interest;
      this.active = true;
      this.insurance = insurance;
   }

   public void finish() {
      this.active = false;
      this.finishDate = LocalDateTime.now();
   }

   public CreditSnapshot toSnapshot() {
      if (id == null) {
         throw new EntityInStateNewException();
      }

      
      return new CreditSnapshot(id, createdAt, startDate, finishDate, version, provision,
         installment, rateOfInterest, interest, active, insurance);
   }

   public void editCredit(LocalDateTime startDate, LocalDateTime finishDate,
      Long provision, Long installment, Double rateOfInterest, Double interest, boolean insurance) {
      this.startDate = startDate;
      this.finishDate = finishDate;
      this.provision = provision;
      this.installment = installment;
      this.rateOfInterest = rateOfInterest;
      this.interest = interest;
      this.insurance = insurance;
   }



   Long getId() {
      return id;
   }
}
