package BANK.domain.credit.exception;

public class CreditNotExistsException
   extends RuntimeException {

   private static final long serialVersionUID = -6409333454995020070L;

   /**
    * Constructs an instance of <code>CreditNotExists</code> with the specified detail message.
    *
    * @param message the detail message.
    */
   public CreditNotExistsException(String message) {
      super(message);
   }

}
