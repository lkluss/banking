package BANK.domain.credit.dto;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author Mateusz.Glabicki
 */
public class CreditSnapshot {

   private final Long id;

   private final LocalDateTime createdAt;
   
   private final LocalDateTime startDate;
   
   private final LocalDateTime finishDate;

   private final long version;
   
   private final Long provision;

   private final Long installment;
   
   private final Double rateOfInterest;
   
   private final Double interest;
   
   private final boolean active;
   
   private final boolean insurance;

   public CreditSnapshot(Long id, LocalDateTime createdAt, LocalDateTime startDate, LocalDateTime finishDate,
      long version, Long provision, Long installment, Double rateOfInterest, Double interest, boolean active,
      boolean insurance) {
      this.id = id;
      this.createdAt = createdAt;
      this.startDate = startDate;
      this.finishDate = finishDate;
      this.version = version;
      this.provision = provision;
      this.installment = installment;
      this.rateOfInterest = rateOfInterest;
      this.interest = interest;
      this.active = active;
      this.insurance = insurance;
   }

   public Long getId() {
      return id;
   }

   public LocalDateTime getCreatedAt() {
      return createdAt;
   }

   public LocalDateTime getStartDate() {
      return startDate;
   }

   public LocalDateTime getFinishDate() {
      return finishDate;
   }

   public long getVersion() {
      return version;
   }

   public Long getProvison() {
      return provision;
   }

   public Long getInstallment() {
      return installment;
   }

   public Double getRateOfInterest() {
      return rateOfInterest;
   }

   public Double getInterest() {
      return interest;
   }

   public boolean isActive() {
      return active;
   }

   public boolean isInsuranace() {
      return insurance;
   }
   
   @Override
   public boolean equals(Object obj) {
      if (!(obj instanceof CreditSnapshot)) {
         return false;
      }
      CreditSnapshot emp = (CreditSnapshot) obj;
      return this.getId().equals(emp.getId());
   }

   @Override
   public int hashCode() {
      int hash = 7;
      hash = 37 * hash + Objects.hashCode(this.id);
      return hash;
   }

}
