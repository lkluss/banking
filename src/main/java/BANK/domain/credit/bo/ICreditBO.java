package BANK.domain.credit.bo;

import java.time.LocalDateTime;
import java.util.UUID;

import BANK.domain.credit.dto.CreditSnapshot;

/**
 *
 * @author Mateusz.Glabicki
 */
public interface ICreditBO {

   CreditSnapshot add(LocalDateTime startDate, LocalDateTime finishDate,
      Long provision, Long installment, Double rateOfInterest, Double interest, boolean insurance);

   CreditSnapshot edit(Long id, LocalDateTime startDate, LocalDateTime finishDate,
      Long provision, Long installment, Double rateOfInterest, Double interest, boolean insurance);

   void delete(Long creditId);

   void finish(Long creditId);
}
