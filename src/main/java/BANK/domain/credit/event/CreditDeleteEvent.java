package BANK.domain.credit.event;

import org.springframework.context.ApplicationEvent;

import BANK.domain.credit.dto.CreditSnapshot;

public class CreditDeleteEvent
   extends ApplicationEvent {

   private static final long serialVersionUID = -2864077050151556923L;

   private final CreditSnapshot creditSnapshot;

   public CreditDeleteEvent(CreditSnapshot creditSnapshot) {
      super(creditSnapshot);
      this.creditSnapshot = creditSnapshot;
   }

   public CreditSnapshot getCreditSnapshot() {
      return creditSnapshot;
   }
}
