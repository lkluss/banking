CREATE TABLE user (
	id BIGINT NOT NULL AUTO_INCREMENT,
	created_at DATETIME NOT NULL,
	firstname VARCHAR(255) NOT NULL,
	guid BINARY (255) NOT NULL,
	lastname VARCHAR(255) NOT NULL,
	mail VARCHAR(255) NOT NULL,
	title VARCHAR(255) NOT NULL,
	username VARCHAR(255) NOT NULL,
    role VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
	version BIGINT NOT NULL,
    enabled bit DEFAULT 1,
    blocked_at DATETIME,
    updated_at DATETIME,
	PRIMARY KEY (id)
);

ALTER TABLE user ADD UNIQUE (guid);


CREATE TABLE Client (
	client_id BIGINT NOT NULL auto_increment,
	created_at DATETIME NOT NULL,
	name VARCHAR(255) NOT NULL,
	surname VARCHAR(255) NOT NULL,
	national_identification_number VARCHAR(255) NOT NULL,
	gender VARCHAR(255) NOT NULL,
	PRIMARY KEY (client_id)
	);

CREATE TABLE Currency (
	currency_id BIGINT NOT NULL auto_increment,
   symbol VARCHAR(3) NOT NULL,
	PRIMARY KEY (currency_id)
	);

CREATE TABLE Brand (
	brand_id BIGINT NOT NULL auto_increment,
   brand_name VARCHAR(256) NOT NULL,
   nationality VARCHAR(256) NOT NULL,
	PRIMARY KEY (brand_id)
	);

CREATE TABLE Account (
	account_id BIGINT NOT NULL auto_increment,
	created_at DATETIME NOT NULL,
   ballance DOUBLE NOT NULL,
   iban VARCHAR(255) NOT NULL,
	PRIMARY KEY (account_id)
	);

CREATE TABLE Transaction (
	transaction_id BIGINT NOT NULL auto_increment,
	created_at DATETIME NOT NULL,
	transaction_date DATETIME NOT NULL,
	from_iban VARCHAR(255) NOT NULL,
	to_ban VARCHAR(255) NOT NULL,
	transaction_value DOUBLE NOT NULL,
	currency BIGINT NOT NULL,
	PRIMARY KEY (transaction_id),
   FOREIGN KEY (currency) REFERENCES Currency(currency_id)
	);

CREATE TABLE Technical_Account (
	technical_account_id BIGINT NOT NULL auto_increment,
	created_at DATETIME NOT NULL,
   ballance DOUBLE NOT NULL,
   transaction_id BIGINT NOT NULL,
	PRIMARY KEY (technical_account_id),
   FOREIGN KEY (transaction_id) REFERENCES Transaction(transaction_id)
	);

CREATE TABLE Insurance (
	insurance_id BIGINT NOT NULL auto_increment,
	created_at DATETIME NOT NULL,
	duration_time DATETIME NOT NULL,
	type VARCHAR(255) NOT NULL,
	contribution BIGINT NOT NULL,
	PRIMARY KEY (insurance_id)
	);

CREATE TABLE Deposit (
	deposit_id BIGINT NOT NULL auto_increment,
	created_at DATETIME NOT NULL,
	const DOUBLE NOT NULL,
	rate BIGINT NOT NULL,
	min_cash DOUBLE NOT NULL,
	max_cash DOUBLE NOT NULL,
	renewable bit NOT NULL,
	capitalization_period BIGINT NOT NULL,
	break_percentage DOUBLE NOT NULL,
	PRIMARY KEY (deposit_id)
	);


CREATE TABLE Credit (
	id BIGINT NOT NULL auto_increment,
	created_at DATETIME NOT NULL,
	start_date DATETIME NOT NULL,
	finish_date DATETIME NOT NULL,
	installment DOUBLE NOT NULL,
	rate_of_interest DOUBLE NOT NULL,
	provision DOUBLE NOT NULL,
	insurance bit NOT NULL,
	active bit NOT NULL,
	interest DOUBLE NOT NULL,
   version BIGINT NOT NULL,
	PRIMARY KEY (id)
	);


CREATE TABLE Active_account (
  	active_account_id bigint(20) NOT NULL auto_increment,
  	client_id bigint(20) NOT NULL,
  	account_id bigint(20) NOT NULL,
  	type varchar(60),
  	PRIMARY KEY (active_account_id),
  	FOREIGN KEY (client_id) REFERENCES Client(client_id),
  	FOREIGN KEY (account_id) REFERENCES Account(account_id)
	);


CREATE TABLE Active_credit (
  	active_credit_id bigint(20) NOT NULL,
  	credit_id bigint(20) NOT NULL,
  	account_id bigint(20) NOT NULL,
  	start_date datetime NOT NULL,
  	PRIMARY KEY (active_credit_id),
  	FOREIGN KEY (credit_id) REFERENCES Credit(id),
  	FOREIGN KEY (account_id) REFERENCES Account(account_id)
  	);


CREATE TABLE Active_deposit (
  	active_deposit_id bigint(20) NOT NULL,
  	deposit_id bigint(20) NOT NULL,
  	account_id bigint(20) NOT NULL,
  	date_start datetime NOT NULL,
  	PRIMARY KEY (active_deposit_id),
  	FOREIGN KEY (deposit_id) REFERENCES Deposit(deposit_id),
  	FOREIGN KEY (account_id) REFERENCES Account(account_id)
	); 



CREATE TABLE Active_insurance (
  	active_insurance_id bigint(20) NOT NULL,
  	insurance_id bigint(20) NOT NULL,
  	account_id bigint(20) NOT NULL,
  	date_start datetime NOT NULL,
  	PRIMARY KEY (active_insurance_id),
  	FOREIGN KEY (insurance_id) REFERENCES Insurance(insurance_id),
  	FOREIGN KEY (account_id) REFERENCES Account(account_id)
	);