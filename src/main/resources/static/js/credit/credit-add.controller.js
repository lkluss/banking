(function () {
   'use strict';

   angular
           .module('bank')
           .controller('CreditAddCtrl', CreditAddCtrl);

   CreditAddCtrl.$inject = [
      '$scope',
      '$state',
      'Credit',
      'CommonUtilService',
      'logToServerUtil',
      'dateResolveUtil',
      'languageUtil'];

   function CreditAddCtrl($scope, $state, Credit, CommonUtilService, logToServerUtil,dateResolveUtil, languageUtil) {
      CommonUtilService.initCommonAndOperator($scope);


      $scope.credit = {
         rateOfInterest: '',
         interest: '',
         insurance: false,
         installment :'',
         provision : '',
         finishDate: '',
         startDate:''
      };
      
      $scope.saveCredit = saveCredit;

      function saveCredit() {
         var credit = new Credit();
         credit.rateOfInterest = $scope.credit.rateOfInterest;
         credit.interest = $scope.credit.interest;
         credit.insurance = $scope.credit.insurance;
         credit.installment = $scope.credit.installment;
         credit.provision = $scope.credit.provision;
         // naprawic to
//         credit.finishDate = dateResolveUtil.formatToISODate($scope.credit.finishDate);
//         credit.startDate = dateResolveUtil.formatToISODate($scope.credit.startDate);

         credit.finishDate = null;
         credit.startDate = null;
         
         credit.$save()
                 .then(function (result) {
                    $state.go('credit.list');
                 }, function (reason) {
                    logToServerUtil.trace('Save Credit failure', reason);
                 });
      }
   }
})();