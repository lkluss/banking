(function () {
   'use strict';

   angular
           .module('bank')
           .controller('CreditEditCtrl', CreditEditCtrl);

   CreditEditCtrl.$inject = [
      '$scope',
      '$state',
      'credit',
      'Credit',
      'CommonUtilService',
      'logToServerUtil',
      'popupService',
      'languageUtil'];

   function CreditEditCtrl($scope, $state,credit, Credit, CommonUtilService, logToServerUtil, popupService, languageUtil) {
      CommonUtilService.initCommonAndOperator($scope);

      $scope.credit = {
         creditId :credit.creditId,
         rateOfInterest : credit.rateOfInterest,
         interest : credit.interest,
         insurance : credit.insurance,
         installment : credit.installment,
         provision : credit.provision,
         finishDate : credit.finishDate,
         startDate : credit.startDate
      };
      $scope.saveCredit = saveCredit;

      function saveCredit() {
         var credit = new Credit();
         credit.rateOfInterest = $scope.credit.rateOfInterest;
         credit.interest = $scope.credit.interest;
         credit.insurance = $scope.credit.insurance;
         credit.installment = $scope.credit.installment;
         credit.provision = $scope.credit.provision;
         credit.finishDate = $scope.credit.finishDate;
         credit.startDate = $scope.credit.startDate;
         credit.creditId = $scope.credit.creditId;   
         credit.$update()
                 .then(function (result) {
                    $state.go('credit.list');
                 }, function (reason) {
                    logToServerUtil.trace('Save Questionnaire Template failure', reason);
                 });
      }
   }
})();