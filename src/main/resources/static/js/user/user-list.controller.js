(function () {
   'use strict';

   angular
           .module('bank')
           .controller('UserListCtrl', UserListCtrl);

   UserListCtrl.$inject = [
      '$scope',
      'User',
      'users',
      '$stateParams',
      '$location',
      '$timeout'];

   function UserListCtrl($scope, User, users, $stateParams, $location, $timeout) {
      $scope.loading = false;
      $scope.users = users;
      $scope.filterRow = true;
      $scope.searchText = [];
      $scope.searchText.enabled = 'false';
      $scope.title = 'breadcrumb.user';

      var firstnameChangeTimeout;
      var lastnameChangeTimeout;

      if ($stateParams.enabled !== undefined) {
         $scope.searchText.enabled = $stateParams.enabled;
      }

      $scope.searchText.firstname = $stateParams.firstname;
      $scope.searchText.lastname = $stateParams.lastname;
      $scope.searchText.title = $stateParams.title;
      $scope.refresh = refresh;

      $scope.updateTitleParam = updateTitleParam;
      $scope.updateStatusParam = updateStatusParam;
      $scope.updateFirstnameParam = updateFirstnameParam;
      $scope.updateLastnameParam = updateLastnameParam;
      $scope.logAction = logAction;
      $scope.blockUser = blockUser;
      $scope.unblockUser = unblockUser;
      $scope.deleteUser = deleteUser;

      var userTitles = [];
      userTitles.sort();
      $scope.userTitles = userTitles;

      function reloadUsers() {
         $scope.loading = true;
         User.query().$promise
                 .then(function (data) {
                    $scope.users = data;
                    $scope.loading = false;
                 });
      }

      function logAction(whonUser, actionName) {
         $scope.loading = true;
         var action = new User();
         action.whonUser = whonUser;
         action.actionName = actionName;
         action.$logAction()
                 .then(function (data) {
                    $scope.loading = false;
                 });
      }

      function blockUser(userId) {
         $scope.loading = true;
         User.block({id: userId}).$promise
                 .then(function (data) {
                    $scope.loading = false;
                    for (var i = 0; i < $scope.users.length; i++) {
                       if ($scope.users[i].userId === userId) {
                          $scope.users[i].enabled = false;
                       }
                    }
                 });
      }

      function deleteUser(userId) {
         $scope.loading = true;
         User.delete({id: userId}).$promise
                 .then(function (data) {
                    $scope.loading = false;
                    for (var i = 0; i < $scope.users.length; i++) {
                       if ($scope.users[i].userId === userId) {
                          $scope.users.splice(i, 1);
                       }
                    }
                 });
      }

      function unblockUser(userId) {
         $scope.loading = true;
         User.unblock({id: userId}).$promise
                 .then(function (data) {
                    for (var i = 0; i < $scope.users.length; i++) {
                       if ($scope.users[i].userId === userId) {
                          $scope.users[i].enabled = true;
                       }
                    }
                    $scope.loading = false;
                 });
      }

      function refresh() {
         User.refresh().$promise
                 .then(function () {
                    reloadUsers();
                    $('#ProcessingModal').modal('hide');
                 });
      }

      function updateTitleParam() {
         $stateParams.title = $scope.searchText.title;
         $location.search($stateParams);
      }
      function updateStatusParam() {
         $stateParams.enabled = $scope.searchText.enabled;
         $location.search($stateParams);
      }
      function updateFirstnameParam() {
         if (firstnameChangeTimeout !== undefined) {
            $timeout.cancel(firstnameChangeTimeout);
         }
         firstnameChangeTimeout = $timeout(updateFirstnameParamAtTimeout, 1000);
      }
      function updateLastnameParam() {
         if (lastnameChangeTimeout !== undefined) {
            $timeout.cancel(lastnameChangeTimeout);
         }
         lastnameChangeTimeout = $timeout(updateLastnameParamAtTimeout, 1000);
      }

      function updateFirstnameParamAtTimeout() {
         if ($scope.searchText.firstname) {
            $stateParams.firstname = $scope.searchText.firstname;
         } else {
            $stateParams.firstname = undefined;
         }
         $location.search($stateParams);
         firstnameChangeTimeout = undefined;
      }
      function updateLastnameParamAtTimeout() {
         if ($scope.searchText.lastname) {
            $stateParams.lastname = $scope.searchText.lastname;
         } else {
            $stateParams.lastname = undefined;
         }
         $location.search($stateParams);
         lastnameChangeTimeout = undefined;
      }
   }
})();