(function () {
   'use strict';

   angular
           .module('bank')
           .controller('UserCommonCtrl', UserCommonCtrl);

   UserCommonCtrl.$inject = ['$scope', 'user'];

   function UserCommonCtrl($scope, user) {
      $scope.user = user;
      $scope.changeView = changeView;
      function changeView(state) {
         $scope.view = {
            details: false
         };
         
         switch (state) {
            case 'DETAILS':
               $scope.view.details = true;
               break;
            default:
               break;
         }
      }
   }
})();
