(function () {
   'use strict';

   angular
           .module('bank')
           .controller('UserEditCtrl', UserEditCtrl);

   UserEditCtrl.$inject = [
      '$scope',
      '$state',
      'user',
      'User',
      'CommonUtilService',
      'logToServerUtil',
      'popupService',
      'languageUtil'];

   function UserEditCtrl($scope, $state,user, User, CommonUtilService, logToServerUtil, popupService, languageUtil) {
      CommonUtilService.initCommonAndOperator($scope);

      $scope.user = {
         firstname: user.firstname,
         lastname: user.lastname,
         userId :user.userId
      };
      $scope.saveUser = saveUser;

      function saveUser() {
         var user = new User();
         user.firstname = $scope.user.firstname;
         user.lastname = $scope.user.lastname;
         user.userId = $scope.user.userId;   
         user.$update()
                 .then(function (result) {
                    $state.go('user.list');
                 }, function (reason) {
                    logToServerUtil.trace('Save Questionnaire Template failure', reason);
                 });
      }
   }
})();