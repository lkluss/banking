(function () {
   'use strict';

   angular
           .module('bank')
           .controller('UserDetailsCtrl', UserDetailsCtrl);

   UserDetailsCtrl.$inject = ['$scope', 'user'];

   function UserDetailsCtrl($scope, user) {
      $scope.user = user;
      $scope.$parent.changeView('DETAILS');
   }
})();
