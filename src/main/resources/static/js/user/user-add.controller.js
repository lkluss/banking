(function () {
   'use strict';

   angular
           .module('bank')
           .controller('UserAddCtrl', UserAddCtrl);

   UserAddCtrl.$inject = [
      '$scope',
      '$state',
      'User',
      'CommonUtilService',
      'logToServerUtil',
      'popupService',
      'languageUtil'];

   function UserAddCtrl($scope, $state, User, CommonUtilService, logToServerUtil, popupService, languageUtil) {
      CommonUtilService.initCommonAndOperator($scope);

      $scope.user = {
         firstname: '',
         lastname: ''
      };
      $scope.saveUser = saveUser;

      function saveUser() {
         var user = new User();
         user.firstname = $scope.user.firstname;
         user.lastname = $scope.user.lastname;

         user.$save()
                 .then(function (result) {
                    $state.go('user.list');
                 }, function (reason) {
                    logToServerUtil.trace('Save Questionnaire Template failure', reason);
                 });
      }
   }
})();