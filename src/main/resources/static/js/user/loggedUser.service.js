(function () {
   'use strict';

   angular
           .module('bank')
           .factory('LoggedUser', LoggedUser);

   LoggedUser.$inject = ['User'];
   
   function LoggedUser(User) {
      var loggedUser = {};
      
      loggedUser.$promise = User.getLoggedUser().$promise;
      loggedUser.$promise
      	.then(function (loadedLoggedUser) {
    	  angular.extend(loggedUser, loadedLoggedUser);
      });
      
      return loggedUser;
   }
})();
