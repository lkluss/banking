(function () {
   'use strict';

   angular
           .module('bank')
           .config(userProvider);

   userProvider.$inject = ['$stateProvider', '$breadcrumbProvider'];

   function userProvider($stateProvider, $breadcrumbProvider) {
      var resolveUsers = ['$state', 'User', 'popupService', 'logToServerUtil', loadUsers];
      var resolveSingleUser = ['$state', '$stateParams', 'User', 'popupService', 'logToServerUtil',
         loadSingleUser];

      $stateProvider
              .state('user', {
                 parent: 'root',
                 url: '/user',
                 abstract: true,
                 template: '<ui-view />'
              })
              .state('user.list', {
                 url: '/list?lastname&firstname&title&enabled',
                 reloadOnSearch: false,
                 templateUrl: '/user/list',
                 controller: 'UserListCtrl',
                 resolve: {
                    users: resolveUsers,
                    translations: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                          $translatePartialLoader.addPart('user/list');
                          return $translate.refresh();
                       }]
                 }
              })
              .state('user.add', {
                 url: '/add',
                 reloadOnSearch: false,
                 templateUrl: '/user/add',
                 controller: 'UserAddCtrl',
                 resolve: {
                    translations: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                          $translatePartialLoader.addPart('user/add');
                          return $translate.refresh();
                       }]
                 }
              })
              .state('user.edit', {
                 url: '/edit/{id:int}',
                 reloadOnSearch: false,
                 templateUrl: '/user/edit',
                 controller: 'UserEditCtrl',
                 resolve: {
                    user: resolveSingleUser,
                    translations: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                          $translatePartialLoader.addPart('user/edit');
                          return $translate.refresh();
                       }]
                 }
              })
              .state('user.common', {
                 url: '/{id:int}',
                 templateUrl: '/user/common',
                 controller: 'UserCommonCtrl',
                 redirectTo: 'user.common.details',
                 resolve: {
                    user: resolveSingleUser,
                    translations: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                          $translatePartialLoader.addPart('user/common');
                          return $translate.refresh();
                       }]
                 }
              })
              .state('user.common.details', {
                 url: '/details',
                 templateUrl: '/user/details',
                 controller: 'UserDetailsCtrl',
                 resolve: {
                    translations: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                          $translatePartialLoader.addPart('user/details');
                          return $translate.refresh();
                       }]
                 }
              });


      function loadUsers($state, User, popupService, logToServerUtil) {
         var usersPromise = User.query().$promise;
         usersPromise
                 .then(function () {
                 }, function (reason) {
                    logToServerUtil.trace('get Users failed', reason);
                    popupService.error('shared.popup.general-failure.title',
                            'shared.popup.general-failure.body');
                    $state.go('dashboard');
                 });
         return usersPromise;
      }

      function loadSingleUser($state, $stateParams, User, popupService, logToServerUtil) {
         var userPromise = User.get({id: $stateParams.id}).$promise;
         userPromise
                 .catch(function (reason) {
                    logToServerUtil.trace('get User failed', reason);
                    popupService.error('shared.popup.general-failure.title',
                            'shared.popup.general-failure.body');
                    $state.go('dashboard');
                 });
         return userPromise;
      }
   }
})();
